import { Module } from "@nestjs/common";
import { TicketModule } from "./ticket/ticket.module";
import { OrganizationModule } from "./organization/organization.module";
import { SectionModule } from "./section/section.module";
import { AuthModule } from "./auth/auth.module";
import { RefreshTokenModule } from "./refresh-token/refresh-token.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { ConnectionOptions } from "typeorm";
import Organization from "./organization/organization.entity";
import RefreshToken from "./refresh-token/refresh-token.entity";
import Section from "./section/section.entity";
import Ticket from "./ticket/ticket.entity";
import { ScheduleModule } from "@nestjs/schedule";

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
        }),
        ScheduleModule.forRoot(),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService): Promise<ConnectionOptions> => ({
                // configService variables are defined inside .env file.
                type: "mysql" as "mysql",
                host:
                    configService.get<string>("NODE_ENV") === "production"
                        ? configService.get<string>("MYSQL_HOST")
                        : "localhost",
                port: +configService.get<string>("MYSQL_PORT"),
                username: configService.get<string>("MYSQL_USER"),
                password: configService.get<string>("MYSQL_PASSWORD"),
                database: configService.get<string>("MYSQL_DATABASE"),
                // registered entities.
                // only entities inside this array are available during runtime.
                entities: [Organization, RefreshToken, Section, Ticket],
                synchronize: true,
            }),
            inject: [ConfigService],
        }),
        TicketModule,
        OrganizationModule,
        SectionModule,
        AuthModule,
        RefreshTokenModule,
    ],
})
export class AppModule {}
