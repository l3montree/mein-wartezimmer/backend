import { Column, Entity, JoinColumn, OneToOne } from "typeorm";
import { AppEntity } from "../app.entity";
import Organization from "../organization/organization.entity";

@Entity()
export default class RefreshToken extends AppEntity {
    @OneToOne(() => Organization, { onDelete: "CASCADE", eager: true })
    @JoinColumn()
    organization: Organization;

    @Column()
    expires: Date;

    @Column({ unique: true })
    token: string;
}
