import { Injectable, UnauthorizedException } from "@nestjs/common";
import * as crypto from "crypto";
import { DeleteResult, Repository } from "typeorm";
import Organization from "../organization/organization.entity";
import RefreshToken from "./refresh-token.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { jwtConstants } from "../auth/constants";
import { Cron } from "@nestjs/schedule";
import * as moment from "moment";

@Injectable()
export class RefreshTokenService {
    constructor(@InjectRepository(RefreshToken) private readonly refreshTokenRepository: Repository<RefreshToken>) {}
    /**
     * Deletes all refresh tokens, which are assigned to the provided organization.
     * @param org
     */
    deleteAllFromOrganization(org: Organization): Promise<DeleteResult> {
        return this.refreshTokenRepository
            .createQueryBuilder()
            .where("organizationId = :orgId", { orgId: org.id })
            .delete()
            .execute();
    }

    /**
     * Creates a new refresh token for the given organization.
     * @param org
     */
    async create(org: Organization): Promise<string> {
        const refreshToken = new RefreshToken();
        refreshToken.organization = org;
        refreshToken.expires = moment()
            .add(jwtConstants.refreshTokenExpiresIn, "hours")
            .toDate();

        refreshToken.token = crypto.randomBytes(48).toString("hex");
        await this.refreshTokenRepository.save(refreshToken);
        // only returning the real token here.
        // everything else is just metadata and not needed client-side.
        return refreshToken.token;
    }

    /**
     * Find a Refresh token by a token string.
     * If the token does not exist, a 403 exception will be thrown.
     * @param {string} token
     * @returns {Promise<RefreshToken>}
     */
    async findOne(token: string): Promise<RefreshToken> {
        const refreshToken = await this.refreshTokenRepository.findOne({ token });
        if (!refreshToken) {
            // throwing a custom unauthorized exception because a 404 error would be misleading.
            throw new UnauthorizedException("Refresh token is not valid.");
        }
        return refreshToken;
    }

    @Cron("0 0 0 * * *")
    cleanup() {
        return this.refreshTokenRepository
            .createQueryBuilder()
            .where("expires < :now", { now: new Date() })
            .delete()
            .execute();
    }
}
