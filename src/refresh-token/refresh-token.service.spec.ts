import { Test, TestingModule } from "@nestjs/testing";
import { RefreshTokenService } from "./refresh-token.service";
import { getRepositoryToken } from "@nestjs/typeorm";
import RefreshToken from "./refresh-token.entity";
import { Repository } from "typeorm";

describe("RefreshTokenService", () => {
    let service: RefreshTokenService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [RefreshTokenService, { provide: getRepositoryToken(RefreshToken), useClass: Repository }],
        }).compile();

        service = module.get<RefreshTokenService>(RefreshTokenService);
    });

    it("should be defined", () => {
        expect(service).toBeDefined();
    });
});
