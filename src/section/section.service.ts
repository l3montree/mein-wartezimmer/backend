import { Injectable } from "@nestjs/common";
import AppService from "../app.service";
import { InjectRepository } from "@nestjs/typeorm";
import Section from "./section.entity";
import { Repository } from "typeorm";

@Injectable()
export class SectionService extends AppService<Section> {
    constructor(@InjectRepository(Section) private readonly sectionRepository: Repository<Section>) {
        super(sectionRepository);
    }
}
