import { Column, Entity, ManyToOne, OneToMany } from "typeorm";
import { AppEntity } from "../app.entity";
import Organization from "../organization/organization.entity";
import Ticket from "../ticket/ticket.entity";

@Entity()
export default class Section extends AppEntity {
    @Column()
    title: string;

    @ManyToOne(
        () => Organization,
        organization => organization.sections,
        { onDelete: "CASCADE" },
    )
    organization: Organization;

    @OneToMany(
        () => Ticket,
        ticket => ticket.section,
    )
    tickets: Promise<Ticket[]>;
}
