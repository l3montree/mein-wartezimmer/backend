// eslint-disable-next-line @typescript-eslint/class-name-casing
export default class _ServiceMock<T> {
    save(entity: T) {
        return entity;
    }
}
