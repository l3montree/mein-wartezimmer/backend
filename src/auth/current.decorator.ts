import { createParamDecorator } from "@nestjs/common";

/**
 * Injects the current organization as a parameter.
 * Requires Passport Authorization.
 * For example: {@link Auth}
 */
export const Current = createParamDecorator((data, req) => {
    const { user } = req;
    if (user) {
        return user;
    }
    // this exception gets thrown, if the User decorator is used, but the route is not decorated with the
    // correct auth guard.
    throw new Error("Organization is not defined. Is the Route decorated with an AuthGuard?");
});
