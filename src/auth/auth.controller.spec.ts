import { Test, TestingModule } from "@nestjs/testing";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";
import _ServiceMock from "../__mocks__/_service.mock";
import { RefreshTokenService } from "../refresh-token/refresh-token.service";

describe("Auth Controller", () => {
    let controller: AuthController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [AuthController],
            providers: [
                { provide: RefreshTokenService, useClass: _ServiceMock },
                { provide: AuthService, useClass: _ServiceMock },
            ],
        }).compile();

        controller = module.get<AuthController>(AuthController);
    });

    it("should be defined", () => {
        expect(controller).toBeDefined();
    });
});
