import { ExtractJwt, Strategy } from "passport-jwt";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { jwtConstants } from "./constants";
import { OrganizationService } from "../organization/organization.service";
import Organization from "../organization/organization.entity";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly organizationService: OrganizationService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: jwtConstants.privateKey,
        });
    }

    async validate(payload: any): Promise<Organization> {
        const organization: Organization = await this.organizationService.findOneByEmail(payload.email);
        if (organization) {
            return organization;
        }
        throw new UnauthorizedException();
    }
}
