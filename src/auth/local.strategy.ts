import { Strategy } from "passport-local";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { AuthService } from "./auth.service";
import Organization from "../organization/organization.entity";

/**
 * Local authorisation strategy.
 * This authorisation strategy is used only to issue a new token
 * {@link AuthController#login}
 */
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService: AuthService) {
        // change the default username field of the passport local strategy to email.
        super({ usernameField: "email" });
    }

    /**
     * Gets called by passport.
     * @param email provided by passport
     * @param password
     */
    async validate(email: string, password: string): Promise<Organization> {
        const organization = await this.authService.validateOrganization(email, password);
        if (!organization) {
            throw new UnauthorizedException();
        }
        return organization;
    }
}
