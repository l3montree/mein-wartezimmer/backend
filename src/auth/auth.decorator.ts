import { applyDecorators, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";

/**
 * Authorization Decorator.
 * Routes decorated with this decorator can use the @Current parameter decorator to retrieve the
 * current logged in user.
 *
 * @param {string} guard Which guard to use. Defaults to jwt based authorization.
 * @param emailNeedsToBeVerified In most cases the email needs to be verified, therefore we are setting the default to true.
 * @constructor
 */
export function Auth(guard = "jwt") {
    return applyDecorators(UseGuards(AuthGuard(guard)));
}
