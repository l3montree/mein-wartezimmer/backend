import { ConflictException, Injectable } from "@nestjs/common";
import * as bcrypt from "bcrypt";
import { OrganizationService } from "../organization/organization.service";
import Organization from "../organization/organization.entity";
import { SignupRequest, Token } from "../shared/auth";
import { jwtConstants } from "./constants";
import { JwtService } from "@nestjs/jwt";
import { RefreshTokenService } from "../refresh-token/refresh-token.service";

@Injectable()
export class AuthService {
    constructor(
        private readonly refreshTokenService: RefreshTokenService,
        private readonly organizationService: OrganizationService,
        private readonly jwtService: JwtService,
    ) {}
    /**
     * Validates a organization by email and password.
     * This method is part of the passport local strategy {@link LocalStrategy}
     * And used during login with credentials.
     * @param email
     * @param password
     */
    async validateOrganization(email: string, password: string): Promise<Organization | null> {
        const organization = await this.organizationService.findOneByEmail(email);
        // verify the given password using bcrypt.
        if (organization && (await bcrypt.compare(password, organization.password))) {
            return organization;
        }
        return null;
    }

    async signup(request: SignupRequest): Promise<Organization> {
        // important:
        // if user is found using the email, it cannot be reused.
        const organization = await this.organizationService.findOneByEmail(request.email);
        if (organization) {
            // email does already exist.
            throw new ConflictException("Email is already registered");
        }
        // hash the password with 10 salt rounds.
        const password = await bcrypt.hash(request.password, 10);
        return this.organizationService.signup({ ...request, password });
    }
    /**
     * Validate the password of a org.
     * @param org
     * @param {string} password
     * @returns {Promise<boolean>}
     */
    validatePassword(org: Organization, password: string): Promise<boolean> {
        return bcrypt.compare(password, org.password);
    }
    /**
     * Hash a password.
     * @param {string} password
     * @returns {Promise<string>}
     */
    hashPassword(password: string): Promise<string> {
        return bcrypt.hash(password, 10);
    }

    /**
     * Issue a new JWT for a organization.
     * Part of the login process.
     * @param org
     */
    async issueToken(org: Organization): Promise<Token> {
        const claims = {
            title: org.title,
            email: org.email,
            id: org.id,
        };

        // first we have to delete all refresh tokens from the given organization.
        // this way, we are making sure, only one single refresh token is valid for a single organization.
        await this.refreshTokenService.deleteAllFromOrganization(org);
        return {
            refreshToken: await this.refreshTokenService.create(org),
            accessToken: this.jwtService.sign(claims),
            tokenType: "Bearer",
            // explicitly using parseInt here, to verify that: '1d' gets parsed to 1.
            expiresIn: 60 * 60 * parseInt(jwtConstants.expiresIn),
        };
    }
}
