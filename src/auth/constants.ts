import * as fs from "fs";

export const jwtConstants = {
    privateKey: fs.readFileSync(`${__dirname}/keys/mein-wartezimmer`),
    publicKey: fs.readFileSync(`${__dirname}/keys/mein-wartezimmer.pub`),

    expiresIn: "1d",
    // using a different time format, due to moment.js and not passport.
    // time unit is 'hours'
    refreshTokenExpiresIn: 31 * 24,
};
