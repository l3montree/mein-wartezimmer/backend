import { Body, Controller, Get, Post, UnauthorizedException } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { Auth } from "./auth.decorator";
import { Current } from "./current.decorator";
import Organization from "../organization/organization.entity";
import { RefreshTokenService } from "../refresh-token/refresh-token.service";
import * as moment from "moment";
import { AuthRoutes } from "../shared/routes";

@Controller("auth")
export class AuthController {
    constructor(private readonly authService: AuthService, private readonly refreshTokenService: RefreshTokenService) {}
    /**
     * Generates a token if the log in was successful.
     * @param org
     */
    @Auth("local")
    @Post("login")
    async login(@Current() org: Organization): Promise<any> {
        return this.authService.issueToken(org);
    }

    /**
     * Signs up a new user.
     * @param {SignupRoute["request"]} requestBody
     * @returns {Promise<SignupRoute["response"]>}
     */
    @Post("signup")
    async signup(@Body() requestBody: AuthRoutes["SIGNUP"]["request"]): Promise<AuthRoutes["SIGNUP"]["response"]> {
        const org = await this.authService.signup(requestBody);
        return this.authService.issueToken(org);
    }

    @Auth("jwt")
    @Get("sync")
    async sync(@Current() org: Organization): Promise<AuthRoutes["SYNC"]["response"]> {
        return {
            ...org,
            sections: await org.sections,
        };
    }

    /**
     * Refreshes an access token by a given refresh token.
     * @throws UnauthorizedException Token not found or token expired.
     * @param {RefreshTokenRoute["request"]} request
     * @returns {Promise<RefreshTokenRoute["response"]>}
     */
    @Post("refresh")
    async issueToken(
        @Body() request: AuthRoutes["REFRESH_TOKEN"]["request"],
    ): Promise<AuthRoutes["REFRESH_TOKEN"]["response"]> {
        const token = await this.refreshTokenService.findOne(request.refreshToken);
        const { organization } = token;
        // check if token is expired or user does not exist anymore.
        if (!organization || moment(token.expires).isBefore(moment())) {
            throw new UnauthorizedException("Refresh token is expired.");
        }
        return this.authService.issueToken(organization);
    }
}
