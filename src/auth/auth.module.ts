import { forwardRef, Module } from "@nestjs/common";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";
import { PassportModule } from "@nestjs/passport";
import { jwtConstants } from "./constants";
import { LocalStrategy } from "./local.strategy";
import { JwtModule } from "@nestjs/jwt";
import { JwtStrategy } from "./jwt.strategy";
import { RefreshTokenModule } from "../refresh-token/refresh-token.module";
import { OrganizationModule } from "../organization/organization.module";

@Module({
    imports: [
        PassportModule,
        RefreshTokenModule,
        forwardRef(() => OrganizationModule),
        JwtModule.register({
            secret: jwtConstants.privateKey,
            signOptions: { expiresIn: jwtConstants.expiresIn },
        }),
    ],
    controllers: [AuthController],
    providers: [AuthService, LocalStrategy, JwtStrategy],
    exports: [AuthService],
})
export class AuthModule {}
