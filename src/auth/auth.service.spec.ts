import { Test, TestingModule } from "@nestjs/testing";
import { AuthService } from "./auth.service";
import { RefreshTokenService } from "../refresh-token/refresh-token.service";
import _ServiceMock from "../__mocks__/_service.mock";
import { OrganizationService } from "../organization/organization.service";
import { JwtService } from "@nestjs/jwt";

describe("AuthService", () => {
    let service: AuthService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                AuthService,
                { provide: JwtService, useClass: _ServiceMock },
                { provide: OrganizationService, useClass: _ServiceMock },
                { provide: RefreshTokenService, useClass: _ServiceMock },
            ],
        }).compile();

        service = module.get<AuthService>(AuthService);
    });

    it("should be defined", () => {
        expect(service).toBeDefined();
    });
});
