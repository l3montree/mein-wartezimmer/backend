import { Test, TestingModule } from "@nestjs/testing";
import { OrganizationController } from "./organization.controller";
import { OrganizationService } from "./organization.service";
import _ServiceMock from "../__mocks__/_service.mock";
import { SectionService } from "../section/section.service";
import { AuthService } from "../auth/auth.service";

describe("Organization Controller", () => {
    let controller: OrganizationController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [OrganizationController],
            providers: [
                { provide: AuthService, useClass: _ServiceMock },
                { provide: OrganizationService, useClass: _ServiceMock },
                { provide: SectionService, useClass: _ServiceMock },
            ],
        }).compile();

        controller = module.get<OrganizationController>(OrganizationController);
    });

    it("should be defined", () => {
        expect(controller).toBeDefined();
    });
});
