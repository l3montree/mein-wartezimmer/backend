import { forwardRef, Module } from "@nestjs/common";
import { OrganizationController } from "./organization.controller";
import { OrganizationService } from "./organization.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import Organization from "./organization.entity";
import { SectionModule } from "../section/section.module";
import { AuthModule } from "../auth/auth.module";

@Module({
    imports: [TypeOrmModule.forFeature([Organization]), SectionModule, AuthModule],
    controllers: [OrganizationController],
    providers: [OrganizationService],
    exports: [OrganizationService],
})
export class OrganizationModule {}
