import { AppEntity } from "../app.entity";
import { Column, Entity, OneToMany } from "typeorm";
import { classToPlain, Exclude } from "class-transformer";
import Section from "../section/section.entity";
import Ticket from "../ticket/ticket.entity";

@Entity()
export default class Organization extends AppEntity {
    @Column()
    title: string;

    @Column()
    address: string;

    @Column()
    lon: string;

    @Column()
    lat: string;

    @Column()
    phoneNumber: string;

    @Column({ nullable: true })
    house_number: string;
    @Column({ nullable: true })
    road: string;
    @Column({ nullable: true })
    town: string;
    @Column({ nullable: true })
    postcode: string;
    @Column({ nullable: true })
    country: string;
    @Column({ nullable: true })
    country_code: string;

    @Column()
    averageTimeNeeded: number;

    @Column({ default: false })
    publicAccessible: boolean;

    @Column({ nullable: true })
    leadTime: number;

    @Exclude({ toPlainOnly: true })
    @Column()
    email: string;

    @Column({ unique: true })
    slug: string;

    @OneToMany(
        () => Section,
        section => section.organization,
    )
    sections: Promise<Section[]>;

    @OneToMany(
        () => Ticket,
        ticket => ticket.organization,
    )
    tickets: Promise<Ticket[]>;

    @Column()
    @Exclude({ toPlainOnly: true })
    password: string;

    toJSON() {
        return classToPlain(this);
    }
}
