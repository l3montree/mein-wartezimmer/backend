import { Injectable } from "@nestjs/common";
import AppService from "../app.service";
import Organization from "./organization.entity";
import { Brackets, Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { SignupRequest } from "../shared/auth";
import Section from "../section/section.entity";
import slugify from "slugify";
import { SectionService } from "../section/section.service";

@Injectable()
export class OrganizationService extends AppService<Organization> {
    constructor(
        private readonly sectionService: SectionService,
        @InjectRepository(Organization) private readonly organizationRepository: Repository<Organization>,
    ) {
        super(organizationRepository);
    }
    /**
     * Finds an organization by email.
     * @param {string} email
     * @returns {Promise<Organization | undefined>}
     */
    findOneByEmail(email: string): Promise<Organization | undefined> {
        return this.organizationRepository.findOne({ email });
    }

    /**
     * Finds an organization by email.
     * @returns {Promise<Organization | undefined>}
     * @param slug
     */
    findOneBySlug(slug: string): Promise<Organization | undefined> {
        return this.organizationRepository.findOne({
            where: {
                slug,
            },
            relations: ["sections"],
        });
    }
    /**
     * Signs a user up.
     * Creates a new user entity and saves it.
     * @param {{email: string; password: string; name: string}} request
     * @returns {Promise<User>}
     */
    async signup(request: SignupRequest): Promise<Organization> {
        const slug = slugify(request.title.toLowerCase());
        const organization = {
            ...request,
            slug,
            sections: Promise.resolve([]),
        };
        let org = null;

        // try to find a unique slug 5 times.
        for (let i = 0; i < 5; i++) {
            try {
                org = await this.organizationRepository.save(organization);
                break;
            } catch (e) {
                organization.slug = `${slug}-${Math.random()
                    .toString(36)
                    .substring(7)
                    .toLowerCase()}`;
            }
        }

        await Promise.all(
            request.sections.map(section => {
                const build = new Section();
                build.title = section;
                build.organization = org;
                return this.sectionService.save(build);
            }),
        );
        return org;
    }

    async search(query: string): Promise<Organization[]> {
        return this.organizationRepository
            .createQueryBuilder("orgs")
            .leftJoinAndSelect("orgs.sections", "sections")
            .where("orgs.publicAccessible = 1")
            .andWhere(
                new Brackets(qb => {
                    const keywords = query.split(" ");
                    keywords.forEach(keyword => {
                        qb.andWhere("orgs.address LIKE :keyword", { keyword: `%${keyword}%` });
                        qb.orWhere("orgs.title LIKE :keyword", { keyword: `%${keyword}%` });
                    });
                }),
            )
            .getMany();
        return Promise.resolve([]);
    }
}
