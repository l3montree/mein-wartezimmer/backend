import { Body, Controller, Get, NotFoundException, Param, Patch, Query } from "@nestjs/common";
import { Auth } from "../auth/auth.decorator";
import { OrganizationRoutes } from "../shared/routes";
import { Current } from "../auth/current.decorator";
import Organization from "./organization.entity";
import { AuthService } from "../auth/auth.service";
import { OrganizationService } from "./organization.service";
import { SectionService } from "../section/section.service";
import Section from "../section/section.entity";

@Controller("organization")
export class OrganizationController {
    constructor(
        private readonly sectionService: SectionService,
        private readonly authService: AuthService,
        private readonly organizationService: OrganizationService,
    ) {}

    @Get("search")
    search(@Query("q") query: string) {
        return this.organizationService.search(query);
    }

    @Get(":slug")
    async get(@Param("slug") slug: string) {
        const org = await this.organizationService.findOneBySlug(slug);
        if (org && org.publicAccessible) {
            return org;
        }
        throw new NotFoundException();
    }

    @Auth()
    @Patch(":organization")
    async patch(
        @Current() org: Organization,
        @Body() req: OrganizationRoutes["PATCH"]["request"],
    ): Promise<OrganizationRoutes["PATCH"]["response"]> {
        const { password, sections, ...rest } = req;
        if (password) {
            // update the password
            org.password = await this.authService.hashPassword(req.password);
        }
        const organization = await this.organizationService.patch(org, rest);
        const existingSections = await org.sections;

        // delete all existing sections, which are not used anymore.
        await Promise.all(
            existingSections
                .filter(sec => sections.findIndex(s => +s.key === sec.id) === -1)
                .map(sec => this.sectionService.delete(sec.id)),
        );
        // update or add the sections.
        const savedSections: Section[] = await Promise.all(
            sections.map(section => {
                if (section.real) {
                    // a real section - the key is the corresponding id.
                    return this.sectionService.patch(+section.key, { title: section.value });
                } else {
                    const build = new Section();
                    build.title = section.value;
                    build.organization = org;
                    return this.sectionService.save(build);
                }
            }),
        );
        return {
            ...organization,
            sections: savedSections,
        };
    }
}
