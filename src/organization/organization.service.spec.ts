import { Test, TestingModule } from "@nestjs/testing";
import { OrganizationService } from "./organization.service";
import { getRepositoryToken } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import Organization from "./organization.entity";
import { SectionService } from "../section/section.service";
import _ServiceMock from "../__mocks__/_service.mock";

describe("OrganizationService", () => {
    let service: OrganizationService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                OrganizationService,
                { provide: SectionService, useClass: _ServiceMock },
                { provide: getRepositoryToken(Organization), useClass: Repository },
            ],
        }).compile();

        service = module.get<OrganizationService>(OrganizationService);
    });

    it("should be defined", () => {
        expect(service).toBeDefined();
    });
});
