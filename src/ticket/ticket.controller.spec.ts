import { Test, TestingModule } from "@nestjs/testing";
import { TicketController } from "./ticket.controller";
import { TicketService } from "./ticket.service";
import _ServiceMock from "../__mocks__/_service.mock";
import { SectionService } from "../section/section.service";
import { OrganizationService } from "../organization/organization.service";

describe("Ticket Controller", () => {
    let controller: TicketController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [TicketController],
            providers: [
                { provide: TicketService, useClass: _ServiceMock },
                { provide: SectionService, useClass: _ServiceMock },
                { provide: OrganizationService, useClass: _ServiceMock },
            ],
        }).compile();

        controller = module.get<TicketController>(TicketController);
    });

    it("should be defined", () => {
        expect(controller).toBeDefined();
    });
});
