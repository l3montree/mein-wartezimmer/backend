import { Column, Entity, ManyToOne } from "typeorm";
import { AppEntity } from "../app.entity";
import Organization from "../organization/organization.entity";
import Section from "../section/section.entity";
import { TicketState, TicketType } from "../shared";

@Entity()
export default class Ticket extends AppEntity {
    @Column()
    title: string;

    @Column()
    state: TicketState;

    @Column({ nullable: true })
    finishedAt: Date;

    @ManyToOne(
        () => Organization,
        organization => organization.tickets,
        { onDelete: "CASCADE", eager: true },
    )
    organization: Organization;

    @Column({ default: TicketType.usual })
    type: TicketType;

    @ManyToOne(
        () => Section,
        section => section.tickets,
        { onDelete: "CASCADE", eager: true },
    )
    section: Section;

    @Column()
    position: number;
}
