import { Module } from "@nestjs/common";
import { TicketController } from "./ticket.controller";
import { TicketService } from "./ticket.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import Ticket from "./ticket.entity";
import { SectionModule } from "../section/section.module";
import { OrganizationModule } from "../organization/organization.module";

@Module({
    imports: [TypeOrmModule.forFeature([Ticket]), SectionModule, OrganizationModule],
    controllers: [TicketController],
    providers: [TicketService],
})
export class TicketModule {}
