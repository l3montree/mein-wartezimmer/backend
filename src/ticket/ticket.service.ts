import { Injectable, Logger } from "@nestjs/common";
import AppService from "../app.service";
import Ticket from "./ticket.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Paginated, TicketState, TicketType } from "../shared";
import * as moment from "moment";

@Injectable()
export class TicketService extends AppService<Ticket> {
    constructor(@InjectRepository(Ticket) private readonly ticketRepository: Repository<Ticket>) {
        super(ticketRepository);
    }
    async save(entity: Ticket): Promise<Ticket> {
        const { max } = await this.ticketRepository
            .createQueryBuilder()
            .where("sectionId = :sectionId", { sectionId: entity.section.id })
            .select("MAX(position)", "max")
            .getRawOne();

        entity.position = (max || 0) + 1;
        return super.save(entity);
    }

    async getPositionAndTime(entity: Ticket): Promise<[number, number]> {
        const items = await this.ticketRepository
            .createQueryBuilder()
            .where("sectionId = :sectionId", { sectionId: entity.section.id })
            .andWhere("position <= :position", { position: entity.position })
            .andWhere("state = :state", { state: TicketState.pending })
            .orderBy("createdAt", "ASC")
            .getMany();
        let time = 0;
        // subtract the current item, while initializing
        let position = -1;
        if (items && items.length > 0) {
            time = Math.ceil(
                moment
                    .duration(
                        moment(entity.createdAt)
                            .utc(true)
                            .diff(moment().utc()),
                    )
                    .asMinutes() -
                    // subtract the asked ticket item.
                    entity.organization.averageTimeNeeded,
            );
        }
        items.forEach(item => {
            switch (item.type) {
                case TicketType.bigDelay:
                    time += TicketType.bigDelay;
                    break;
                case TicketType.mediumDelay:
                    time += TicketType.mediumDelay;
                    break;
                case TicketType.smallDelay:
                    time += TicketType.smallDelay;
                    break;
                case TicketType.usual:
                    time += entity.organization.averageTimeNeeded;
                    position++;
                    break;
            }
        });
        return [position, time];
    }

    async listPaginated(
        options: { sectionId?: number; organizationId: number; state: TicketState; averageTimeNeeded: number },
        page: number = 0,
        skip: number = 0,
        take: number = 0,
    ): Promise<Paginated<Omit<Ticket, "saveSetting" | "mergeSetting" | "getSetting" | "merge">>> {
        const query = this.ticketRepository
            .createQueryBuilder("ticket")
            .leftJoinAndSelect("ticket.section", "section")
            .where("ticket.state = :state", { state: options.state })
            .andWhere("ticket.organizationId = :organizationId", { organizationId: options.organizationId });

        if (options.sectionId) {
            query.andWhere("ticket.sectionId = :sectionId", { sectionId: options.sectionId });
        }

        const [content, amount] = await query
            .orderBy("ticket.position")
            .skip(skip)
            .take(take)
            .getManyAndCount();
        return this.toPaginated(content, amount, page, take);
    }

    async moveToStart(ticket: Ticket) {
        const { min } = await this.ticketRepository
            .createQueryBuilder()
            .where("sectionId = :sectionId", { sectionId: ticket.section.id })
            .select("MIN(position)", "min")
            .getRawOne();
        ticket.position = min - 1;
        return this.ticketRepository.save(ticket);
    }

    async moveToEnd(ticket: Ticket) {
        const { max } = await this.ticketRepository
            .createQueryBuilder()
            .where("sectionId = :sectionId", { sectionId: ticket.section.id })
            .select("MAX(position)", "max")
            .getRawOne();
        ticket.position = max + 1;
        return this.ticketRepository.save(ticket);
    }

    async moveBefore(ticketToMove: Ticket, moveBeforeTicket: Ticket) {
        const positionToMoveTo = moveBeforeTicket.position;
        this.ticketRepository
            .createQueryBuilder()
            .where("position < :positionToMoveTo", { positionToMoveTo })
            .andWhere("id != :id", { id: ticketToMove.id })
            .andWhere("sectionId = :sectionId", { sectionId: ticketToMove.section.id })
            .update()
            .set({ position: () => "position - 1" })
            .execute();

        this.ticketRepository
            .createQueryBuilder()
            .where("position >= :positionToMoveTo", { positionToMoveTo })
            .andWhere("id != :id", { id: ticketToMove.id })
            .andWhere("sectionId = :sectionId", { sectionId: ticketToMove.section.id })
            .update()
            .set({ position: () => "position + 1" })
            .execute();

        ticketToMove.position = positionToMoveTo;
        return this.ticketRepository.save(ticketToMove);
    }

    async moveAfter(ticketToMove: Ticket, moveBeforeTicket: Ticket) {
        const positionToMoveTo = moveBeforeTicket.position;
        this.ticketRepository
            .createQueryBuilder()
            .where("position <= :positionToMoveTo", { positionToMoveTo })
            .andWhere("id != :id", { id: ticketToMove.id })
            .andWhere("sectionId = :sectionId", { sectionId: ticketToMove.section.id })
            .update()
            .set({ position: () => "position - 1" })
            .execute();

        this.ticketRepository
            .createQueryBuilder()
            .where("position > :positionToMoveTo", { positionToMoveTo })
            .andWhere("id != :id", { id: ticketToMove.id })
            .andWhere("sectionId = :sectionId", { sectionId: ticketToMove.section.id })
            .update()
            .set({ position: () => "position + 1" })
            .execute();

        ticketToMove.position = positionToMoveTo;
        return this.ticketRepository.save(ticketToMove);
    }
}
