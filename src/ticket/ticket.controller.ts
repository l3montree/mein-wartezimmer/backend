import { Body, Controller, Delete, Get, NotFoundException, Param, Patch, Post, Query } from "@nestjs/common";
import { Auth } from "../auth/auth.decorator";
import { Current } from "../auth/current.decorator";
import Organization from "../organization/organization.entity";
import { TicketRoutes } from "../shared/routes";
import Ticket from "./ticket.entity";
import { SortFlag, Ticket as SharedTicket, TicketState, TicketType } from "../shared";
import { TicketService } from "./ticket.service";
import { SectionService } from "../section/section.service";
import { Section } from "../shared/auth";
import { OrganizationService } from "../organization/organization.service";

@Controller()
export class TicketController {
    constructor(
        private readonly organizationService: OrganizationService,
        private readonly ticketService: TicketService,
        private readonly sectionService: SectionService,
    ) {}

    @Get("tickets/:ticketId")
    async get(
        @Param("ticketId") ticketId: string,
    ): Promise<SharedTicket & { time: number; position: number; organization: Organization; section: Section }> {
        const ticket = await this.ticketService.findOneById(+ticketId);
        if (!ticket || ticket.state !== TicketState.pending) {
            throw new NotFoundException();
        }
        const [position, time] = await this.ticketService.getPositionAndTime(ticket);

        return {
            ...ticket,
            position: position,
            time,
        };
    }

    @Auth("jwt")
    @Post("/organizations/:organization/tickets")
    async storePrivate(
        @Current() org: Organization,
        @Body() req: TicketRoutes["POST"]["request"],
    ): Promise<TicketRoutes["POST"]["response"]> {
        return this.store(org, req);
    }

    private async store(org: Organization, req: TicketRoutes["POST"]["request"]) {
        const build = new Ticket();
        const section = await this.sectionService.findOneById(req.sectionId);
        build.organization = org;
        build.title = req.title;
        build.section = section;
        build.state = TicketState.pending;
        build.type = req.type ?? TicketType.usual;
        const ticket = await this.ticketService.save(build);
        const [_, time] = await this.ticketService.getPositionAndTime(ticket);
        return { ...ticket, time };
    }

    @Post("/public/organizations/:organization/tickets")
    async storePublic(
        @Param("organization") orgId: string,
        @Body() req: TicketRoutes["POST"]["request"],
    ): Promise<TicketRoutes["POST"]["response"]> {
        const org = await this.organizationService.findOneById(+orgId);
        if (org && org.publicAccessible) {
            return this.store(org, req);
        }
        throw new NotFoundException();
    }

    @Auth("jwt")
    @Delete("/organizations/:organization/tickets/:ticketId")
    async destroy(@Current() organization: Organization, @Param("ticketId") ticketId: string) {
        const ticket = await this.ticketService.findOneById(+ticketId);
        if (ticket && ticket.organization.id === organization.id) {
            if (ticket.state === TicketState.deleted || ticket.type !== TicketType.usual) {
                return this.ticketService.delete(ticket.id);
            } else {
                return this.ticketService.patch(ticket, { state: TicketState.deleted });
            }
        }
        throw new NotFoundException();
    }

    @Auth("jwt")
    @Patch("/organizations/:organization/tickets/:ticketId")
    async patch(
        @Current() org: Organization,
        @Param("ticketId") ticketId: string,
        @Body() req: TicketRoutes["PATCH"]["request"],
    ) {
        const ticket = await this.ticketService.findOneById(+ticketId);
        if (ticket && ticket.organization.id === org.id) {
            const patched = await this.ticketService.patch(ticket, req);
            if ("state" in req) {
                return this.ticketService.moveToEnd(patched);
            }
            return patched;
        }
        throw new NotFoundException();
    }

    @Auth("jwt")
    @Get("organizations/:organization/tickets")
    list(
        @Current() org: Organization,
        @Query("page") page: string,
        @Query("perPage") perPage: string,
        @Query("sectionId") sectionId?: string,
        @Query("state") state?: TicketState,
    ) {
        return this.ticketService.listPaginated(
            {
                averageTimeNeeded: org.averageTimeNeeded,
                sectionId: +sectionId,
                organizationId: org.id,
                state: state || TicketState.pending,
            },
            +page,
            +page * 30,
            30,
        );
    }

    @Auth("jwt")
    @Patch("organizations/:organization/tickets/:ticket/move-before/:beforeTicket")
    async moveBefore(
        @Param("organization") organizationId: string,
        @Param("ticket") ticketId: string,
        @Param("beforeTicket") beforeTicketId: string,
    ) {
        const ticket = await this.ticketService.findOneById(+ticketId);
        if (+beforeTicketId === SortFlag.first) {
            return this.ticketService.moveToStart(ticket);
        } else if (+beforeTicketId === SortFlag.last) {
            return this.ticketService.moveToEnd(ticket);
        }

        const beforeTicket: Ticket = await this.ticketService.findOneById(+beforeTicketId);
        if (beforeTicket.section.id !== ticket.section.id) {
            return;
        }
        return this.ticketService.moveBefore(ticket, beforeTicket);
    }

    @Auth("jwt")
    @Patch("organizations/:organization/tickets/:ticket/move-after/:afterTicket")
    async moveAfter(
        @Param("organization") organizationId: string,
        @Param("ticket") ticketId: string,
        @Param("afterTicket") afterTicketId: string,
    ) {
        const ticket = await this.ticketService.findOneById(+ticketId);
        if (+afterTicketId === SortFlag.first) {
            return this.ticketService.moveToStart(ticket);
        } else if (+afterTicketId === SortFlag.last) {
            return this.ticketService.moveToEnd(ticket);
        }

        const afterTicket: Ticket = await this.ticketService.findOneById(+afterTicketId);
        if (afterTicket.section.id !== ticket.section.id) {
            return;
        }
        return this.ticketService.moveAfter(ticket, afterTicket);
    }
}
