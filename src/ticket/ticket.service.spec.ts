import { Test, TestingModule } from "@nestjs/testing";
import { TicketService } from "./ticket.service";
import { getRepositoryToken } from "@nestjs/typeorm";
import Ticket from "./ticket.entity";
import { Repository } from "typeorm";

describe("TicketService", () => {
    let service: TicketService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [TicketService, { provide: getRepositoryToken(Ticket), useClass: Repository }],
        }).compile();

        service = module.get<TicketService>(TicketService);
    });

    it("should be defined", () => {
        expect(service).toBeDefined();
    });
});
