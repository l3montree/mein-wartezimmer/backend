import { AppEntity } from "./app.entity";
import { DeleteResult, ObjectLiteral, Repository } from "typeorm";
import { Paginated } from "./shared";

/**
 * TODO: Switch parameters of patch methods.
 */
export default abstract class AppService<Entity extends AppEntity & ObjectLiteral> {
    protected constructor(public repository: Repository<Entity>) {}

    /**
     * Saves a provided entity.
     * @param {Entity} entity
     * @returns {Promise<Entity>}
     */
    save(entity: Entity): Promise<Entity> {
        return this.repository.save(entity);
    }

    /**
     * Find a single entity instance by a given id.
     * @param {AppEntity["id"]} id
     * @returns {Promise<Entity>}
     */
    findOneById(id: Entity["id"]): Promise<Entity> {
        return this.repository.findOne(id);
    }

    /**
     * Patch an entity.
     * Overwrites all properties of entity which are defined in patch.
     * @param {Entity} entity Entity to patch.
     * @param {Partial<Entity>} patch New Values - Overwrites existing ones.
     * @returns {Promise<Entity>}
     */
    patch(entity: Entity, patch: Partial<Entity>): Promise<Entity>;

    /**
     * Patch an entity by a given Id.
     * First retrieves the entity, then patches it.
     * @param {Entity["id"]} entityId
     * @param {Partial<Entity>} patch
     * @returns {Promise<Entity>}
     */
    patch(entityId: Entity["id"], patch: Partial<Entity>): Promise<Entity>;

    /**
     * Implementation.
     * @param {Entity["id"] | Entity} entityOrId
     * @param {Partial<Entity>} patch
     * @returns {Promise<Entity>}
     */
    async patch(entityOrId: Entity["id"] | Entity, patch: Partial<Entity>): Promise<Entity> {
        const entity: Entity = typeof entityOrId === "number" ? await this.repository.findOne(entityOrId) : entityOrId;
        Object.keys(patch).forEach(key => {
            if (entity.hasOwnProperty(key)) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
                // @ts-ignore
                entity[key] = patch[key];
            }
        });
        return this.repository.save(entity);
    }

    /**
     * Deletes all entities by ids.
     * @param {number[]} entityIds Ids of the entities to delete.
     * @returns {Promise<DeleteResult>}
     */
    delete(...entityIds: Entity["id"][]): Promise<DeleteResult> {
        return this.repository.delete(entityIds);
    }

    /**
     * Updates all entities to match the given patch object.
     * @param {Partial<Entity>} patch New values of the entities.
     * @param {Entity["id"]} entityIds IDS of the entity to update.
     * @returns {Promise<UpdateResult>}
     */
    patchAll(patch: Partial<Entity>, ...entityIds: Entity["id"][]) {
        return this.repository
            .createQueryBuilder()
            .update()
            .set(patch)
            .whereInIds(entityIds)
            .execute();
    }

    refresh(entity: Entity) {
        return this.findOneById(entity.id);
    }

    protected toPaginated<T>(content: T[], amount: number, page: number, perPage: number): Paginated<T> {
        return {
            content,
            numberOfElements: content.length,
            empty: content.length === 0,
            totalPages: Math.floor(amount / perPage),
            totalElements: amount,
            currentPage: page,
        };
    }

    findByIds(entityIds: Entity["id"][]): Promise<Entity[]> {
        return this.repository.findByIds(entityIds);
    }
}
