import { Column, CreateDateColumn, ObjectLiteral, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
/**
 * Base entity for all entities inside the project.
 * Defines standard columns like id and created and updated at timestamps.
 * Additionally adds a meta column of JSON type to save settings.
 */
export abstract class AppEntity<Settings = Record<string, any>> implements ObjectLiteral {
    /**
     * Unique identifier of the Entity.
     */
    @PrimaryGeneratedColumn()
    id: number;

    /**
     * Timestamp
     */
    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    /**
     * Timestamp
     */
    @UpdateDateColumn()
    updatedAt: Date;

    /**
     * Meta column
     * Allows to get defined via provided settings API.
     */
    @Column({ type: "json", nullable: true })
    meta?: Settings;

    /**
     * Get a setting by Key.
     * @param key Name of the setting to retrieve. Only upper level supported.
     * TODO: Add support for nested setting.
     */
    public getSetting<K extends keyof Settings>(key: K): Settings[K] | undefined;
    /**
     * Get a setting by Key.
     * @param key Name of the setting to retrieve. Only upper level supported.
     * @param d Default value if the setting does not exist.
     */
    public getSetting<D, K extends keyof Settings>(key: K, d: D): Settings[K] | D;
    public getSetting(key: string, d?: any) {
        return this.meta[key] ?? d;
    }

    /**
     * Save a single setting
     * @param key Name of the setting.
     * @param value Value to save.
     * @param overwrite Flag, if the setting should get overwritten if it does already exists. Defaults to true.
     */
    public saveSetting<Key extends keyof Settings>(key: Key, value: Settings[Key], overwrite = true): void {
        if (this.meta[key] && overwrite) {
            this.meta[key] = value;
        } else if (!this.meta[key]) {
            this.meta[key] = value;
        }
    }

    /**
     * Merge two settings objects into one.
     * The parameter gets prioritized.
     * => If the parameter includes same keys as the already defined settings,
     * the parameter values will overwrite the already defined ones.
     * @param toMerge
     */
    public mergeSetting(toMerge: Partial<Settings>) {
        this.meta = { ...this.meta, ...toMerge };
    }

    /**
     * Merges two entities.
     * The toMerge object will overwrite any properties, that are already defined.
     * @param {Partial<this>} toMerge
     * @param {boolean} force If this flag is true, it will just merge the two objects, without checking if the values do exist in the first place.
     */
    public merge(toMerge: Partial<this>, force = false) {
        Object.keys(toMerge).forEach(key => {
            if (key in this && !force) {
                this[key] = toMerge[key];
            } else if (force) {
                this[key] = toMerge[key];
            }
        });
    }
}
