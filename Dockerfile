# Production DOCKERFILE
# Maintainer: Tim Bastin <tim.bastin>

FROM node:13-alpine
LABEL maintainer="bastin.tim@gmail.com"

RUN apk --no-cache add g++ make libpng-dev

WORKDIR /usr/app/

ENV NODE_ENV production
ENV PORT 3000

EXPOSE 3000

COPY . .

RUN apk --no-cache add --virtual native-deps \
  g++ gcc libgcc libstdc++ linux-headers make python && \
  npm install --quiet node-gyp -g &&\
  npm install --quiet && \
  apk del native-deps

RUN npm run build

CMD [ "npm", "run", "start:prod" ]
